package dynamics;

import java.util.LinkedList;
import java.util.List;

public class HitBoard extends Board {
	private final int MISSED = PINNED;
	private final int HIT = MISSED + 1;

	public HitBoard() {
		super();
	}

	public void hit(Cell cell) {
		grid[cell.row][cell.column] = HIT;
	}

	public void miss(Cell cell) {
		pin(cell);
	}

	public boolean isHit(Cell cell) {
		return grid[cell.row][cell.column] == HIT;
	}

	public List<Cell> findHits() {
		List<Cell> cells = new LinkedList<>();
		for (int row = 0; row < getSide(); row++) {
			for (int col = 0; col < getSide(); col++) {
				Cell cell = new Cell(row, col);
				if (isHit(cell)) {
					cells.add(cell);
				}
			}
		}
		return cells;
	}

}
