package dynamics;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ShipHunter {
	private Board board;
	private int[][] heatMap;
	private List<Ship> fleet = new LinkedList<>();
	private final Cell a1 = new Cell(0, 0);

	public ShipHunter(Board board) {
		this.board = board;
		int side = board.getSide();
		heatMap = new int[side][side];

		addShipToFleet(2);
		addShipToFleet(3);
		addShipToFleet(3);
		addShipToFleet(4);
		addShipToFleet(5);
	}

	private void addShipToFleet(int length) {
		fleet.add(new Ship(a1, length, false));
		fleet.add(new Ship(a1, length, true));
	}

	public List<Cell> findBestCells() {
		reset();
		raiseHeatFromFleet();
		return findHottestCells();
	}

	private void reset() {
		for (int i = 0; i < board.getSide(); i++)
			Arrays.fill(heatMap[i], 0);

		for (Ship ship : fleet)
			ship.moveTo(a1);
	}

	private void raiseHeatFromFleet() {
		for (Ship ship : fleet) {
			raiseHeatAtAllValidPositions(ship);
		}
	}

	private void raiseHeatAtAllValidPositions(Ship ship) {
		for (int row = a1.row; row < board.getSide(); row++) {
			for (int column = a1.column; column < board.getSide(); column++) {
				ship.moveTo(new Cell(row, column));
				raiseHeatFromShip(ship);
			}
		}
	}

	private void raiseHeatFromShip(Ship ship) {
		if (board.isInsideBoard(ship) && avoidsAllPins(ship)) {
			for (Cell cell : ship.getOccupation()) {
				++heatMap[cell.row][cell.column];
			}
		}
	}

	private boolean avoidsAllPins(Ship ship) {
		for (Cell cell : ship.getOccupation()) {
			if (board.isPinned(cell)) {
				return false;
			}
		}
		return true;
	}

	private List<Cell> findHottestCells() {
		List<Cell> bestCells = new LinkedList<Cell>();
		int bestScore = 0;
		for (int row = 0; row < board.getSide(); row++) {
			for (int col = 0; col < board.getSide(); col++) {
				int current = heatMap[row][col];
				if (current > bestScore) {
					bestScore = current;
					bestCells.clear();
				}

				if (current == bestScore) {
					bestCells.add(new Cell(row, col));
				}
			}
		}
		return bestCells;
	}

}
