package dynamics;

public class Board {
	public Board() {
		grid = new int[SIDE][SIDE];
	}

	public int getSide() {
		return SIDE;
	}

	public boolean isPinned(final Cell cell) {
		return grid[cell.row][cell.column] >= PINNED;
	}

	public void pin(final Cell cell) {
		grid[cell.row][cell.column] = PINNED;
	}

	public boolean isValidCell(Cell cell) {
		return cell.row < SIDE && cell.column < SIDE && cell.row >= 0 && cell.column >= 0;
	}

	public boolean isInsideBoard(Ship ship) {
		for (Cell cell : ship.getOccupation()) {
			if (!isValidCell(cell)) {
				return false;
			}
		}
		return true;
	}

	protected final int PINNED = 1;
	protected int[][] grid;
	private final int SIDE = 10;
}
