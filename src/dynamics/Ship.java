package dynamics;

import java.util.LinkedList;
import java.util.List;

public class Ship {
	private Cell position;
	public final int size;
	public final boolean isHorizontal;

	public Ship(Cell cell, int size, boolean isHorizontal) {
		this.position = cell;
		this.size = size;
		this.isHorizontal = isHorizontal;
	}

	public Cell end() {
		Cell lastCell;
		if (isHorizontal) {
			lastCell = new Cell(position.row, position.column + size - 1);
		} else {
			lastCell = new Cell(position.row + size - 1, position.column);
		}
		return lastCell;
	}

	public List<Cell> getOccupation() {
		List<Cell> cells = new LinkedList<>();
		for (int i = 0; i < size; i++) {
			if (isHorizontal) {
				cells.add(new Cell(position.row, position.column + i));
			} else {
				cells.add(new Cell(position.row + i, position.column));
			}
		}
		return cells;
	}

	public boolean occupies(Cell cell) {
		List<Cell> cells = getOccupation();
		return cells.contains(cell);
	}

	public void moveTo(Cell cell) {
		position = cell;
	}

}
