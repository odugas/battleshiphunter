package dynamics;

import java.util.LinkedList;
import java.util.List;

public class ShipDestroyer {
	private HitBoard board;

	public ShipDestroyer(HitBoard board) {
		this.board = board;
	}

	public List<Cell> findBestCells() {
		List<Cell> bestCells = new LinkedList<>();
		List<Cell> hitCells = board.findHits();
		for (Cell cell : hitCells) {
			bestCells.addAll(findValidNeighbors(cell));
		}
		return bestCells;
	}

	private List<Cell> findValidNeighbors(Cell cell) {
		List<Cell> allNeighbors = new LinkedList<>();
		allNeighbors.add(new Cell(cell.row + 1, cell.column));
		allNeighbors.add(new Cell(cell.row - 1, cell.column));
		allNeighbors.add(new Cell(cell.row, cell.column + 1));
		allNeighbors.add(new Cell(cell.row, cell.column - 1));

		List<Cell> validNeighbors = new LinkedList<>();
		for (Cell neighbor : allNeighbors) {
			if (board.isValidCell(neighbor) && !board.isPinned(neighbor)) {
				validNeighbors.add(neighbor);
			}
		}
		return validNeighbors;
	}
}
