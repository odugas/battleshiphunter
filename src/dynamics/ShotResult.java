package dynamics;

public enum ShotResult {
	Missed, Hit, Sunk, InvalidShot;
}
