package service;

import java.util.List;
import java.util.Random;

import dynamics.Cell;
import dynamics.HitBoard;
import dynamics.ShipDestroyer;
import dynamics.ShipHunter;

public class TargetingSystem {
	private HitBoard board;
	private ShipHunter hunter;
	private ShipDestroyer destroyer;
	private Random randomizer;

	public TargetingSystem(HitBoard board, ShipHunter hunter, ShipDestroyer destroyer) {
		this.board = board;
		this.hunter = hunter;
		this.destroyer = destroyer;
		randomizer = new Random(Double.doubleToLongBits(Math.random()));
	}

	public Cell selectTarget() {
		List<Cell> targets = hunter.findBestCells();
		targets.addAll(destroyer.findBestCells());

		return targets.get(randomizer.nextInt(targets.size()));
	}

	public void miss(Cell cell) {
		board.miss(cell);
	}

	public void hit(Cell cell) {
		board.hit(cell);
	}
}
