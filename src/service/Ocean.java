package service;

import java.util.LinkedList;
import java.util.List;

import dynamics.Board;
import dynamics.Cell;
import dynamics.Ship;
import dynamics.ShotResult;

public class Ocean {
	private List<Ship> fleet;
	private Board board;

	public Ocean(Board board) {
		this.fleet = new LinkedList<>();
		this.board = board;
	}

	public ShotResult shoot(Cell cell) {
		if (!board.isValidCell(cell))
			return ShotResult.InvalidShot;
		if (board.isPinned(cell))
			return ShotResult.InvalidShot;

		board.pin(cell);
		for (Ship ship : fleet) {
			if (ship.occupies(cell)) {
				if (isSunk(ship))
					return ShotResult.Sunk;
				else
					return ShotResult.Hit;
			}
		}
		return ShotResult.Missed;

	}

	private boolean isSunk(Ship ship) {
		boolean allPinned = true;
		for (Cell occupiedCell : ship.getOccupation())
			allPinned = allPinned && board.isPinned(occupiedCell);

		return allPinned;
	}

	public boolean isValid() {
		boolean hasProblemWithAShip = false;
		for (Ship ship : fleet) {
			hasProblemWithAShip = !board.isInsideBoard(ship) || crossesOtherShips(ship);
		}
		return !hasProblemWithAShip;
	}

	public void add(Ship ship) {
		fleet.add(ship);
	}

	private boolean crossesOtherShips(final Ship ship) {
		boolean overlap = false;
		for (Cell cell : ship.getOccupation()) {
			for (Ship otherShip : fleet) {
				overlap = overlap || (ship != otherShip && otherShip.occupies(cell));
			}
		}

		return overlap;
	}

}
