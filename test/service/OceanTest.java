package service;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

import dynamics.Board;
import dynamics.Cell;
import dynamics.Ship;
import dynamics.ShotResult;

import service.Ocean;

public class OceanTest {

	private Board board = mock(Board.class);

	private Ocean ocean;
	private final boolean horizontal = true;
	private final boolean vertical = false;
	private final Cell tooFarBelow = new Cell(10, 0);
	private final Cell tooFarRight = new Cell(0, 10);

	@Before
	public void setup() {
		ocean = new Ocean(board);
		when(board.isValidCell(any(Cell.class))).thenReturn(true);
		when(board.isInsideBoard(any(Ship.class))).thenReturn(true);
		when(board.isValidCell(tooFarBelow)).thenReturn(false);
		when(board.isValidCell(tooFarRight)).thenReturn(false);
	}

	@Test
	public void emptyOcean_ShouldBeValid() {
		assertTrue(ocean.isValid());
	}

	@Test
	public void addOneShip_givenEnoughSpace_ThenOceanShouldBeValid() {
		addShip(0, 0, 2, horizontal);

		assertTrue(ocean.isValid());
	}

	@Test
	public void addOneHorizontalBoat_ThatGoesOutsideOcean_ThenOceanShouldBeInvalid() {
		when(board.isInsideBoard(any(Ship.class))).thenReturn(false);
		addShip(0, 8, 3, horizontal);

		assertFalse(ocean.isValid());
	}

	@Test
	public void addOneVerticalBoat_ThatGoesOutsideOcean_ThenShouldBeInvalid() {
		when(board.isInsideBoard(any(Ship.class))).thenReturn(false);
		addShip(8, 0, 3, vertical);

		assertFalse(ocean.isValid());
	}

	@Test
	public void crossingShips_shouldInvalidateOcean() {
		addShip(1, 2, 3, vertical);
		addShip(2, 1, 3, horizontal);
		assertFalse(ocean.isValid());
	}

	@Test
	public void overlappingShips_shouldInvalidateOcean() {
		addShip(1, 2, 3, vertical);
		addShip(3, 2, 3, horizontal);
		assertFalse(ocean.isValid());
	}

	@Test
	public void manyValidShips_GivenNoCrossNorOverlap_shouldBeValid() {
		fillTheOcean();
		assertTrue(ocean.isValid());
	}

	@Test
	public void shoot_unoccupiedCell_shouldReturnMiss() {
		fillTheOcean();
		Cell emptyCell = new Cell(9, 0);

		ShotResult result = ocean.shoot(emptyCell);

		assertEquals(ShotResult.Missed, result);
	}

	@Test
	public void shoot_invalidCell_shouldReturnInvalid() {
		Cell tooFarAbove = new Cell(-1, 0);
		Cell tooFarLeft = new Cell(0, -1);
		when(board.isValidCell(tooFarAbove)).thenReturn(false);
		when(board.isValidCell(tooFarLeft)).thenReturn(false);

		assertEquals(ShotResult.InvalidShot, ocean.shoot(tooFarRight));
		assertEquals(ShotResult.InvalidShot, ocean.shoot(tooFarLeft));
		assertEquals(ShotResult.InvalidShot, ocean.shoot(tooFarAbove));
		assertEquals(ShotResult.InvalidShot, ocean.shoot(tooFarBelow));
	}

	@Test
	public void shoot_sameSpotTwice_shouldReturnInvalid() {
		fillTheOcean();
		Cell cell = new Cell(9, 0);
		when(board.isPinned(cell)).thenReturn(true);

		ShotResult result = ocean.shoot(cell);

		assertEquals(ShotResult.InvalidShot, result);
	}

	@Test
	public void shoot_hitAShip_ShouldReturnHit() {
		fillTheOcean();
		Cell occupiedCell = new Cell(5, 5);

		ShotResult result = ocean.shoot(occupiedCell);

		assertEquals(ShotResult.Hit, result);
	}

	@Test
	public void shoot_hitLastCellOfShip_ShouldReturnSunk() {
		fillTheOcean();
		Cell firstCellOfSmallShip = new Cell(0, 2);
		Cell lastCellOfShip = new Cell(1, 2);
		when(board.isPinned(firstCellOfSmallShip)).thenReturn(false, true);
		ocean.shoot(firstCellOfSmallShip);

		when(board.isPinned(lastCellOfShip)).thenReturn(false, true);
		ShotResult result = ocean.shoot(lastCellOfShip);

		assertEquals(ShotResult.Sunk, result);
	}

	private void fillTheOcean() {
		addShip(0, 2, 2, vertical);
		addShip(2, 0, 3, horizontal);
		addShip(2, 3, 3, horizontal);
		addShip(5, 5, 4, vertical);
		addShip(9, 5, 5, horizontal);
	}

	private void addShip(int row, int col, int size, boolean isHorizontal) {
		Ship ship = new Ship(new Cell(row, col), size, isHorizontal);
		ocean.add(ship);
	}
}
