package service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import dynamics.Cell;
import dynamics.HitBoard;
import dynamics.ShipDestroyer;
import dynamics.ShipHunter;
import service.TargetingSystem;

public class TargetingSystemTest {
	private HitBoard board = mock(HitBoard.class);
	private ShipHunter hunter = mock(ShipHunter.class);
	private ShipDestroyer destroyer = mock(ShipDestroyer.class);

	List<Cell> huntedCells = new LinkedList<>();
	List<Cell> neighborCells = new LinkedList<>();

	private TargetingSystem targeter;

	@Before
	public void setup() {
		when(hunter.findBestCells()).thenReturn(huntedCells);
		when(destroyer.findBestCells()).thenReturn(neighborCells);

		targeter = new TargetingSystem(board, hunter, destroyer);
	}

	@Test
	public void givenNoHit_nextTarget_shouldBeWithinHuntedSelection() {
		huntedCells.add(new Cell(0, 0));
		huntedCells.add(new Cell(5, 5));

		Cell target = targeter.selectTarget();

		assertTrue(huntedCells.contains(target));
	}

	@Test
	public void givenHit_nextTarget_shouldBeFromOneOfEither() {
		huntedCells.add(new Cell(5, 5));
		neighborCells.add(new Cell(7, 7));

		Cell target = targeter.selectTarget();

		assertTrue(huntedCells.contains(target) || neighborCells.contains(target));
	}

	@Test
	public void hit_shouldCallBoard() {
		Cell cell = new Cell(5, 5);
		targeter.hit(cell);
		verify(board).hit(cell);
	}

	@Test
	public void miss_shouldCallBoard() {
		Cell cell = new Cell(5, 5);
		targeter.miss(cell);
		verify(board).miss(cell);
	}

}
