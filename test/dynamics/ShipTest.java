package dynamics;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

public class ShipTest {
	private Ship ship;
	private final boolean VERTICAL = false;
	private final boolean HORIZONTAL = true;

	@Test
	public void horizontalShip_endPosition_shouldAddSizeToRow() {
		final int row = 1;
		final int column = 2;
		final int size = 3;
		ship = new Ship(new Cell(row, column), size, HORIZONTAL);

		Cell end = ship.end();

		assertEquals(row, end.row);
		assertEquals(column + size - 1, end.column);
	}

	@Test
	public void verticalShip_endPosition_shouldAddSizeToColumn() {
		final int row = 1;
		final int column = 2;
		final int size = 3;
		ship = new Ship(new Cell(row, column), size, VERTICAL);

		Cell end = ship.end();

		assertEquals(row + size - 1, end.row);
		assertEquals(column, end.column);
	}

	@Test
	public void verticalShip_getOccupation_shouldContainEachCellsFromStartToEnd() {
		List<Cell> cells = new LinkedList<>();
		cells.add(new Cell(1, 1));
		cells.add(new Cell(2, 1));
		cells.add(new Cell(3, 1));
		cells.add(new Cell(4, 1));
		ship = new Ship(cells.get(0), 4, VERTICAL);

		List<Cell> occupation = ship.getOccupation();

		for (Cell cell : occupation) {
			assertTrue(cells.contains(cell));
		}
	}

	@Test
	public void horizontalShip_getOccupation_shouldContainEachCellsFromStartToEnd() {
		List<Cell> cells = new LinkedList<>();
		cells.add(new Cell(1, 1));
		cells.add(new Cell(1, 2));
		cells.add(new Cell(1, 3));
		cells.add(new Cell(1, 4));
		ship = new Ship(cells.get(0), 4, HORIZONTAL);

		List<Cell> occupation = ship.getOccupation();

		for (Cell cell : occupation) {
			assertTrue(cells.contains(cell));
		}
	}

	@Test
	public void givenOccupiedCell_occupies_shouldReturnTrue() {
		List<Cell> cells = new LinkedList<>();
		cells.add(new Cell(1, 1));
		cells.add(new Cell(2, 1));
		cells.add(new Cell(3, 1));
		cells.add(new Cell(4, 1));
		ship = new Ship(cells.get(0), 4, VERTICAL);

		for (Cell cell : cells)
			assertTrue(ship.occupies(cell));
	}

	@Test
	public void givenUnoccupiedCell_occupies_shouldReturnFalse() {
		List<Cell> cells = new LinkedList<>();
		cells.add(new Cell(1, 2));
		cells.add(new Cell(1, 0));
		cells.add(new Cell(5, 1));
		cells.add(new Cell(0, 1));
		ship = new Ship(new Cell(1, 1), 4, VERTICAL);

		for (Cell cell : cells)
			assertFalse(ship.occupies(cell));
	}

	@Test
	public void movingShip_changesItsOccupation() {
		ship = new Ship(new Cell(1, 1), 4, VERTICAL);
		List<Cell> cells = new LinkedList<>();
		cells.add(new Cell(1, 2));
		cells.add(new Cell(2, 2));
		cells.add(new Cell(3, 2));
		cells.add(new Cell(4, 2));

		ship.moveTo(cells.get(0));

		for (Cell cell : cells)
			assertTrue(ship.occupies(cell));
	}
}
