package dynamics;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class BoardTest {
	private Board board;
	private Cell cell;

	@Before
	public void setup() {
		board = new Board();
		cell = new Cell(0, 0);
	}

	@Test
	public void emptyCell_shouldBeMarkedAsUnoccupied() {
		assertFalse(board.isPinned(cell));
	}

	@Test
	public void emptyCell_whenPinned_shouldBeMarkedAsOccupied() {
		board.pin(cell);
		assertTrue(board.isPinned(cell));
	}

	@Test
	public void pinnedCell_cannotBeUnpinned() {
		board.pin(cell);
		board.pin(cell);
		assertTrue(board.isPinned(cell));
	}

	@Test
	public void outerCells_shouldBeInvalid() {
		Cell tooFarRight = new Cell(0, 10);
		Cell tooFarLeft = new Cell(0, -1);
		Cell tooFarAbove = new Cell(-1, 0);
		Cell tooFarBelow = new Cell(10, 0);

		assertFalse(board.isValidCell(tooFarRight));
		assertFalse(board.isValidCell(tooFarLeft));
		assertFalse(board.isValidCell(tooFarAbove));
		assertFalse(board.isValidCell(tooFarBelow));
	}

	@Test
	public void ship_outsideBoard_shouldBeDetectedAsOutside() {
		Ship ship = new Ship(new Cell(0, 8), 3, true);
		assertFalse(board.isInsideBoard(ship));
	}

	@Test
	public void ship_insideBoard_shouldBeDetectedAsInside() {
		Ship ship = new Ship(new Cell(8, 7), 3, true);
		assertTrue(board.isInsideBoard(ship));
	}
}
