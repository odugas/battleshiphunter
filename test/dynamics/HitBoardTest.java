package dynamics;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class HitBoardTest {
	private HitBoard board;
	private Cell cell;

	@Before
	public void setup() {
		board = new HitBoard();
		cell = new Cell(0, 0);
	}

	@Test
	public void hit_shouldBePinned() {
		board.hit(cell);
		assertTrue(board.isPinned(cell));
	}

	@Test
	public void hit_shouldBeHit() {
		board.hit(cell);
		assertTrue(board.isHit(cell));
	}

	@Test
	public void miss_shouldBePinned() {
		board.miss(cell);
		assertTrue(board.isPinned(cell));
	}

	@Test
	public void miss_shouldNotBeHit() {
		board.miss(cell);
		assertFalse(board.isHit(cell));
	}

	@Test
	public void findHits_shouldReturnEveryHitCells() {
		Cell anotherCell = new Cell(3, 5);
		board.hit(cell);
		board.hit(anotherCell);

		List<Cell> hitCells = board.findHits();

		assertTrue(hitCells.contains(cell));
		assertTrue(hitCells.contains(anotherCell));
		assertEquals(2, hitCells.size());
	}
}
