package dynamics;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class ShipDestroyerTest {

	private HitBoard board = new HitBoard();
	private ShipDestroyer destructor;
	private List<Cell> expected = new LinkedList<>();
	private List<Cell> received;

	@Before
	public void setup() {
		destructor = new ShipDestroyer(board);
	}

	@Test
	public void noRegisteredHits_search_shouldReturnNoCells() {
		received = destructor.findBestCells();

		allExpectedCellsAreReturned();
	}

	@Test
	public void oneRegisteredHit_search_shouldReturnAllNeighbors() {
		Cell cell = new Cell(5, 5);
		addExpected(cell.row - 1, cell.column);
		addExpected(cell.row + 1, cell.column);
		addExpected(cell.row, cell.column - 1);
		addExpected(cell.row, cell.column + 1);
		board.hit(cell);

		received = destructor.findBestCells();

		allExpectedCellsAreReturned();
	}

	@Test
	public void hitACorner_search_shouldReturnValidCells() {
		Cell cell = new Cell(0, 0);
		addExpected(cell.row + 1, cell.column);
		addExpected(cell.row, cell.column + 1);
		board.hit(cell);

		received = destructor.findBestCells();

		allExpectedCellsAreReturned();
	}

	@Test
	public void alreadyPinnedNeighbors_shouldBeExcluded_fromSearch() {
		Cell cell = new Cell(5, 5);
		addExpected(cell.row + 1, cell.column);
		addExpected(cell.row, cell.column - 1);
		board.hit(cell);
		board.pin(new Cell(cell.row - 1, cell.column));
		board.pin(new Cell(cell.row, cell.column + 1));

		received = destructor.findBestCells();

		allExpectedCellsAreReturned();
	}

	private void addExpected(int row, int column) {
		expected.add(new Cell(row, column));
	}

	private void allExpectedCellsAreReturned() {
		assertEquals(expected.size(), received.size());
		for (Cell cell : received)
			assertTrue(expected.contains(cell));
	}
}
