package dynamics;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class ShipHunterTest {
	private Board board = new Board();
	private ShipHunter searcher;
	private List<Cell> expected = new LinkedList<>();
	private List<Cell> received;

	@Before
	public void setup() {
		searcher = new ShipHunter(board);
	}

	@Test
	public void givenNoPinnedCell_search_shouldReturnCenterCells() {
		addExpected(4, 4);
		addExpected(4, 5);
		addExpected(5, 4);
		addExpected(5, 5);

		received = searcher.findBestCells();

		allExpectedCellsAreReturned();
	}

	@Test
	public void givenD5Pinned_thenSearch_shouldReturnF6Only() {
		board.pin(new Cell(4, 4));
		addExpected(5, 5);

		received = searcher.findBestCells();

		allExpectedCellsAreReturned();
	}

	private void addExpected(int row, int column) {
		expected.add(new Cell(row, column));
	}

	private void allExpectedCellsAreReturned() {
		assertEquals(expected.size(), received.size());
		for (Cell cell : received)
			assertTrue(expected.contains(cell));
	}

}
